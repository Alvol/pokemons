/**
 * Created by VoloshynPC on 4/18/2016.
 */
angular.module('pokedexApp').filter('typeFilter',function(){
    var service = {};
    var visibleType = '';

    service.setTypeFilter = function(typeFilter){
        visibleType = typeFilter;
        service.doFilter();
    };

    service.doFilter = function(pokemon){
        var res = true;
        if (pokemon && visibleType && visibleType!='') {
            for (var i = 0; i < pokemon.types.length; i++) {
                var type = pokemon.types[i];
                if (type.name.indexOf(this.visibleType.toLocaleLowerCase()) != -1) {
                    res = true;
                } else {
                    res = false;
                }
            }
        } else {
            res = true;
        }
        return res;
    };

    return service;
});
