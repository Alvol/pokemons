angular.module('pokedexApp').factory('pokemonsDelegate', function () {
    var services = {};
    var pokemonsList = [];
    var baseUrl = 'http://pokeapi.co';
    var nextPage = '/api/v1/pokemon/?limit=12';
    
    services.getPokemons = function(){
        return pokemonsList;
    };
    
    services.loadPokemons = function ($http) {
        $http.get(baseUrl+nextPage).
        success(function (data) {
            pokemonsList.push.apply(pokemonsList, data.objects);
            nextPage = data.meta.next;
            console.log("load pokemons: ", pokemonsList);
        }).error(function (data, status) {
            console.log("error: ", data, status);
        });
    };
    
    services.loadPokemons = function($http){
        $http.get(baseUrl+nextPage).
        success(function (data) {
                pokemonsList.push.apply(pokemonsList, data.objects);
                nextPage = data.meta.next;
                console.log("load more pokemons: ", pokemonsList);
        }).error(function (data) {
            console.log("error: ", data, status);
        });
    };

    return services;
});