var pokedexApp = angular.module('pokedexApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'ngAnimate']);

var AppCtrl = pokedexApp.controller('pokedexCtrl', function ($http, pokemonsDelegate) {
        this.title = 'Pokedex';
        this.selectedPokemon = null;
        this.visibleType = '';
        this.loading = false;
        this.avatarPath = 'http://pokeapi.co/media/img/';

        this.loadPokemons = function () {
            this.selectedPokemon = null;
            this.loading = true;
            pokemonsDelegate.loadPokemons($http);
            this.loading = false;
        };
        this.loadPokemons();

        this.getPokemons = function () {
            return pokemonsDelegate.getPokemons();
        };

        this.pokemonClick = function (pokemon) {
            this.selectedPokemon = pokemon;
        };

        this.getDetailsClass = function (last) {
            var cssClass = last ? null : "typeDetails";
            return cssClass;
        };

        this.filterTypeClick = function (typeName) {
            this.visibleType = ''
            typeFilterFilter.setTypeFilter(typeName);
            this.typeFilter();
        };
    

        this.clearFilter = function () {
            this.visibleType = '';
        };

        this.getTypeClass = function (name) {
            var cssClass = null;
            switch (name) {
                case 'normal':
                    cssClass = "type normal";
                    break;

                case 'fire':
                    cssClass = "type fire";
                    break;

                case 'grass':
                    cssClass = "type grass";
                    break;

                case 'poison':
                    cssClass = "type poison";
                    break;

                case 'water':
                    cssClass = "type water";
                    break;

                case 'bug':
                    cssClass = "type bug";
                    break;

                case 'flying':
                    cssClass = "type flying";
                    break;

                case 'ground':
                    cssClass = "type ground";
                    break;

                case 'electric':
                    cssClass = "type electric";
                    break;

                case 'fairy':
                    cssClass = "type fairy";
                    break;

                case 'fighting':
                    cssClass = "type fighting";
                    break;

                default:
                    console.log('new type ' + name);
                    cssClass = "type normal";
            }
            return cssClass;
        }
    }
);